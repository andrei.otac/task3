<?php include 'header.php'; ?>

    <div class="task3 fill">

        <div class="grid__container">

            <h1 class="title">Pixel Art</h1>

            <div class="tools">

                <div class="tools__bucket tools__item" id="bucket">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M21.143 9.667c-.733-1.392-1.914-3.05-3.617-4.753-2.977-2.978-5.478-3.914-6.785-3.914-.414 0-.708.094-.86.246l-1.361 1.36c-1.899-.236-3.42.106-4.294.983-.876.875-1.164 2.159-.792 3.523.492 1.806 2.305 4.049 5.905 5.375.038.323.157.638.405.885.588.588 1.535.586 2.121 0s.588-1.533.002-2.119c-.588-.587-1.537-.588-2.123-.001l-.17.256c-2.031-.765-3.395-1.828-4.232-2.9l3.879-3.875c.496 2.73 6.432 8.676 9.178 9.178l-7.115 7.107c-.234.153-2.798-.316-6.156-3.675-3.393-3.393-3.175-5.271-3.027-5.498l1.859-1.856c-.439-.359-.925-1.103-1.141-1.689l-2.134 2.131c-.445.446-.685 1.064-.685 1.82 0 1.634 1.121 3.915 3.713 6.506 2.764 2.764 5.58 4.243 7.432 4.243.648 0 1.18-.195 1.547-.562l8.086-8.078c.91.874-.778 3.538-.778 4.648 0 1.104.896 1.999 2 1.999 1.105 0 2-.896 2-2 0-3.184-1.425-6.81-2.857-9.34zm-16.209-5.371c.527-.53 1.471-.791 2.656-.761l-3.209 3.206c-.236-.978-.049-1.845.553-2.445zm9.292 4.079l-.03-.029c-1.292-1.292-3.803-4.356-3.096-5.063.715-.715 3.488 1.521 5.062 3.096.862.862 2.088 2.247 2.937 3.458-1.717-1.074-3.491-1.469-4.873-1.462z"/>
                    </svg>
                </div>

                <div class="tools__item active" id="fill">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M18.247 6.763c-1.215 2.033-2.405 4.002-3.056 5.023-.82 1.287-1.102 1.471-2.748 2.142l-.173-.145c.363-1.752.491-2.063 1.606-3.106.882-.823 2.598-2.351 4.371-3.914zm-10.269 11.254l.871.727c-.16 2.549-2.361 3.452-4.231 3.222 1.736-1.604 1.924-3.639 3.36-3.949zm14.746-17.831s-7.887 6.857-10.213 9.03c-1.838 1.719-1.846 2.504-2.441 5.336l2.016 1.681c2.671-1.099 3.44-1.248 4.793-3.373 1.713-2.687 7.016-11.698 7.016-11.698.422-.748-.516-1.528-1.171-.976zm-14.098 15.767c-5.093.053-3.121 5.901-8.626 5.445 3.252 4.523 11.38 3.041 10.832-3.604l-2.206-1.841z"/>
                    </svg>
                </div>

            </div>

            <h1 class="grid__title">Grid Size</h1>

            <div class="grid__block">


                <div class="grid__item active" data-size="8">8x8</div>
                <div class="grid__item" data-size="12">12x12</div>
                <div class="grid__item" data-size="16">16x16</div>
                <div class="grid__item" data-size="32">32x32</div>

            </div>

            <h1 class="color__title">Select Color</h1>

            <div class="color__block">


                <div class="color__item active" data-color="#f70700"></div>
                <div class="color__item" data-color="#8c0b8b"></div>
                <div class="color__item" data-color="#7efe00"></div>
                <div class="color__item" data-color="#021afe"></div>
                <div class="color__item" data-color="#fefe00"></div>
                <div class="color__item" data-color="#fba506"></div>
                <div class="color__item" data-color="#ffffff"></div>
                <div class="color__item" data-color="#000000"></div>
                <div class="color__item" data-color="#fee5c4"></div>
                <div class="color__item" data-color="#fcc0cc"></div>
                <div class="color__item" data-color="#d2691d"></div>

            </div>

            <div class="pixel__block pixel__block-gird8"></div>

            <div class="preview__button">Preview</div>

            <div class="preview__block">

                <div class="preview__title">Your Image</div>
                <div class="preview__image"></div>
                <a class="preview__download" href="#">Download</a>

                <div class="preview__close">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="white" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M20.197 2.837l.867.867-8.21 8.291 8.308 8.202-.866.867-8.292-8.21-8.23 8.311-.84-.84 8.213-8.32-8.312-8.231.84-.84 8.319 8.212 8.203-8.309zm-.009-2.837l-8.212 8.318-8.31-8.204-3.666 3.667 8.321 8.24-8.207 8.313 3.667 3.666 8.237-8.318 8.285 8.204 3.697-3.698-8.315-8.209 8.201-8.282-3.698-3.697z"/>
                    </svg>
                </div>

            </div>

        </div>

    </div>


<?php include 'footer.php'; ?>