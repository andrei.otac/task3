<!doctype html>
<html class="no-js">
<head class="">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/assets/css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/general.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/task3.css"/>


</head>

<body>