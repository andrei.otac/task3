console.log('it works!');

(function ($) { //create closure so we can safely use $ as alias for jQuery

    // Task 3
    var selectedG = '8', selectedC = '#f70700', selectedT = 'fill';


    $(document).ready(function () {


        // Task 3
        let gridEl = '';
        for (let i = 0; i < selectedG * selectedG; i++) {
            gridEl += '<div class="pixel__item elem' + i + '"></div>';
        }
        $('.pixel__block').append(gridEl);

        $(".color__block .color__item").each(function (index) {
            let colorI = $(this);
            $(this).css("background-color", colorI.attr('data-color'));
            $(this).css("height", colorI.width());
        });

        $(".pixel__block .pixel__item").each(function (index) {
            let pixelI = $(this);
            $(this).css("height", pixelI.width());
        });

        $(".grid__block .grid__item").click(function () {
            selectedG = $(this).attr('data-size');
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            $('.pixel__block .pixel__item').remove();
            let gridEl = '';

            for (let i = 0; i < selectedG * selectedG; i++) {
                gridEl += '<div class="pixel__item elem' + i + '"></div>';
            }
            $('.pixel__block').append(gridEl);
            $('.pixel__block').attr('class', 'pixel__block');
            $(".pixel__block").addClass('pixel__block-grid' + selectedG);
            $(".pixel__block .pixel__item").each(function (index) {
                let pixelI = $(this);
                $(this).css("height", pixelI.width());
            });
        });

        $(".color__block .color__item").click(function () {
            selectedC = $(this).attr('data-color');
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
        });

        $(".preview__close").click(function () {
            $('.preview__block').removeClass('active');
        });

        $(document).on("click", ".pixel__block .pixel__item", function () {
            let elem = $('.pixel__block .pixel__item').index(this);
            let color = $(this).css('background-color');
            if ($('.task3').hasClass('bucket')) {
                checkNeighbor(elem, color, selectedC);
            }
            $(this).css({backgroundColor: selectedC});
        });

        $(".tools__item").click(function () {
            selectedT = $(this).attr('id');
            $('.task3').attr('class', 'task3 ' + selectedT);
            $(".tools__item").removeClass('active');
            $(this).addClass('active');
        });

        // Create Canvas
        var element = $(".pixel__block");
        var getCanvas;
        $(".preview__button").on('click', function () {
            $(".preview__image canvas").remove();
            html2canvas(element, {
                onrendered: function (canvas) {
                    $(".preview__image").append(canvas);
                    getCanvas = canvas;
                }
            });
            $(".preview__block").addClass('active');
        });

        // Download the canvas
        $(".preview__download").on('click', function () {
            var imgageData =
                getCanvas.toDataURL("image/png ");

            var newData = imgageData.replace(
                /^data:image\/png/, "data:application/octet-stream");

            $(".preview__download").attr(
                "download", "pixelArtByAndrei.png").attr(
                "href", newData);
        });
    });

    // Task 3
    function checkNeighbor(element, color, newColor) {
        if ($('.elem' + (parseInt(element) - parseInt(selectedG))).css("background-color") == color) {
            $('.elem' + (parseInt(element) - parseInt(selectedG))).css("background-color", newColor);
            checkNeighbor((parseInt(element) - parseInt(selectedG)), color, newColor);
        }

        if ($('.elem' + (parseInt(element) - 1)).css("background-color") == color) {
            $('.elem' + (parseInt(element) - 1)).css("background-color", newColor);
            checkNeighbor((parseInt(element) - 1), color, newColor);
        }

        if ($('.elem' + (parseInt(element) + 1)).css("background-color") == color) {
            $('.elem' + (parseInt(element) + 1)).css("background-color", newColor);
            checkNeighbor((parseInt(element) + 1), color, newColor);
        }

        if ($('.elem' + (parseInt(element) + parseInt(selectedG))).css("background-color") == color) {
            $('.elem' + (parseInt(element) + parseInt(selectedG))).css("background-color", newColor);
            checkNeighbor((parseInt(element) + parseInt(selectedG)), color, newColor);
        }
    }

})(jQuery); // add everything that uses jQuery above this line
